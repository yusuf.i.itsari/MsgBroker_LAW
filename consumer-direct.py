#!/usr/bin/env python
import pika
import sys

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='LOAD_BALANCE', exchange_type='direct')

result = channel.queue_declare(exclusive=True)
queue_name = result.method.queue

severities = sys.argv[1:]
if not severities:
    sys.stderr.write("Usage: %s [info] [warning] [error]\n" % sys.argv[0])
    sys.exit(1)

for severity in severities:
    channel.queue_bind(exchange='LOAD_BALANCE', queue=queue_name, routing_key=severity)

print(' [*] Waiting for logs. To exit press CTRL+C')

def callback(ch, method, properties, body):
    print(" [x] %r:%r" % (method.routing_key, body))

channel.basic_consume(callback, queue=queue_name, no_ack=True)

channel.start_consuming()