#!/usr/bin/env python
import pika
import sys

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='LOAD_BALANCE', exchange_type='direct')

severity = 'LB'
message = 'Hello World!'

channel.basic_publish(exchange='LOAD_BALANCE', routing_key=severity, body=message)
print(" [x] Sent %r:%r" % (severity, message))
connection.close()