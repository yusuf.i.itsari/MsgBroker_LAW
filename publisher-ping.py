#!/usr/bin/env python
import pika
import time
import datetime
import json

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='PING', exchange_type='fanout')

while True:
	timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
	message = json.dumps({'action':'ping', 'npm':'1406578275', 'ts':timestamp})
	channel.basic_publish(exchange='PING',routing_key='',body=message)
	print(" [x] Sent %r" % message)
	time.sleep(5)
