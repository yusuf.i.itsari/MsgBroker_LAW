#!/usr/bin/env python
import pika
import sys

credentials = pika.PlainCredentials('1406578275','1406578275')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='152.118.148.103', port=5672, virtual_host='/1406578275', credentials=credentials))
channel = connection.channel()

channel.exchange_declare(exchange='TOPIK', exchange_type='topic')

message = ['msg1','msg2','msg3']
routing_key = ['chat.topik1','chat.topik2','chat.topik3']

for x in range(0, 3):
    channel.basic_publish(exchange='TOPIK', routing_key=routing_key[x], body=message[x])
    print(" [x] Sent %r:%r" % (routing_key[x], message[x]))

connection.close()